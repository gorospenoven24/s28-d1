console.log("Hello world");

// Asynchornize statement
//  The fetch API allow to asynchronously request from a resource data
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

console.log("goodbye");

fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

// Retrieve content data the from the response object
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((json) => console.log(json));


// "async" and "await"
async function fetchData(){

	// waits for te "fetch" method to complete the stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	// result return by fetc is a returned promise
	console.log(result);
	// The return "response" is an object
	console.log(typeof result);
	// To access the content of the "response",body property should be diretly accesed
	console.log(result.body);

	let json = await result.json()
	console.log(json)
}
fetchData();


// Getting a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1').then((response) => response .json()).then((json) => console.log(json));



// Creating a post
/*syntax:
	fetch('URL', options).then((response) =>{}).then ((response)+>{})
*/
fetch('https://jsonplaceholder.typicode.com/posts',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body:'Hello world',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// Put Method
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body:'Hello again',
		userID: 1
	})

}).then((response) => response.json()).then((json) => console.log(json));



// Usdate a post Using PATCH method
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Corrected post',
		
	})

}).then((response) => response.json()).then((json) => console.log(json));


// Deleting a post

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
});